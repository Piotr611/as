////////////////////////////////////////////////////////////////////////////////
//
// File: can_drv.c
// Description: for ATmega32m1
// Author(s): RiSuS as an engineer "SKB WEST"
// Copyright(s):
// Version:
// Creation date: 25-June-2015
//
////////////////////////////////////////////////////////////////////////////////

#include "can_drv.h"
#include "canhandler.h"

CanMsg rxMsg;

//
//	Initialization CAN
//
void init_can(void)
{
	CANGCON = ( 1 << SWRES );   	// Software reset
	CANTCON = 0x00;         		// CAN timing prescaler set to 0;					
	CANBT1 = 0x06;      			// Set baud rate to 250kb (assuming 16Mhz IOclk)
	CANBT2 = 0x0C;
	CANBT3 = 0x37;

	for ( int8_t mob=0; mob<6; mob++ ) {  
		CANPAGE = ( mob << 4 );     // Selects Message Object 0-5
		CANCDMOB = 0x00;       		// Disable mob
		CANSTMOB = 0x00;     		// Clear mob status register;
		
		CANIDM1 = 0x00;   	// Clear Mask, let all IDs pass    
		CANIDM2 = 0x00; 	// ""
		CANIDM3 = 0x00; 	// ""
		CANIDM4 = 0x00; 	// ""    
	}
	
	CANPAGE = 0;		// Selecto MOB0
	CANCDMOB = ( 1 << CONMOB1) | ( 1 << IDE ) | ( 8 << DLC0);  // Enable Reception 29 bit IDE DLC8
	
	CANIE2  = ( 1 << IEMOB0 );   									// Enable interrupts on mob0 for reception and transmission
	CANGIE = ( 1 << ENIT ) | ( 1 << ENRX ) /*| ( 1 << ENTX )*/;   // Enable interrupts on receive
	CANGCON |= ( 1 << ENASTB );								   // Enable mode. CAN channel enters in enable mode once 11 recessive bits have been read
}

//
// Transmit
//
void can_tx(CanMsg *msg) 
{
	int8_t mob = 0, counter = 10;
	
	// Wait free mob
	while(!mob && counter) {
		for(int8_t i=1; i<6; i++) {
			if(!(CANEN2 & ( 1 << i ))) {
				mob = i;
				break;
			}
		}
		counter--;
	}
	if(!counter) {return;}
	
	// Select MOb for transmission
	CANPAGE = (mob << 4);

	CANSTMOB = 0x00;    	// Clear mob status register

	if (msg->ide == CAN_ID_EXT) {
		CANIDT1   = CAN_SET_EXT_ID_28_21(msg->id);
		CANIDT2   = CAN_SET_EXT_ID_20_13(msg->id);
		CANIDT3   = CAN_SET_EXT_ID_12_5( msg->id);
		CANIDT4   = CAN_SET_EXT_ID_4_0(  msg->id);
	} else if (msg->ide == CAN_ID_STD) {
		CANIDT1   = CAN_SET_STD_ID_10_4(msg->id);
		CANIDT2   = CAN_SET_STD_ID_3_0( msg->id);		
	} else
		return;

	for ( uint8_t i = 0; i < msg->dlc; i++ )
		CANMSG = msg->data[i];

	CANCDMOB = ( 1 << CONMOB0 ) | ( msg->ide << IDE ) | ( msg->dlc << DLC0 ); 	// Enable transmission

	while ( ! ( CANSTMOB & ( 1 << TXOK ) ) ) {
		if(CANGIT&(1<<OVRTIM))  //����� �� ������������ �������. ������ ����� ����� ���� CAN
			break;
	};	// wait for TXOK flag set

	CANCDMOB = 0x00;	// Disable Transmission
	CANSTMOB = 0x00;	// Clear TXOK flag
}

//
//	Reception
//
ISR (CAN_INT_vect)
{
	int8_t length, savecanpage;
	
	savecanpage = CANPAGE;
	CANPAGE = CANHPMOB & 0xF0;				// Selects MOB with highest priority interrupt

	// Interrupt caused by receive finished
	if ( CANSTMOB & ( 1 << RXOK) ) {
		length = ( CANCDMOB & 0x0F );		// DLC, number of bytes to be received
		rxMsg.dlc = length;
		for ( int8_t i = 0; i < length; i++ )
			rxMsg.data[i] = CANMSG; 		// Get data, INDX auto increments CANMSG
		rxMsg.ide = ((CANCDMOB & (1 << IDE))?(1):(0));
		if (rxMsg.ide) {
			Can_get_ext_id (rxMsg.id);
		} else {
			Can_get_std_id (rxMsg.id);
		}
		CANCDMOB = (( 1 << CONMOB1 ) | ( 1 << IDE ) | ( 8 << DLC0));  // Enable Reception 29 bit IDE DLC8
	}
	
	CANSTMOB = 0x00; 			// Reset reason on selected channel
	CANPAGE = savecanpage;		// Restore original MOB
	// ������� ���������
	canMainHandler(&rxMsg);
}