////////////////////////////////////////////////////////////////////////////////
//
// File: canhandler.h
// Description: skv_bu_mega16m1
// Author(s): RiSuS as an engineer "SKB WEST"
// Copyright(s):
// Version:
// Creation date: 2-July-2015
//
////////////////////////////////////////////////////////////////////////////////

#ifndef __CAN_HANDLER_H
#define __CAN_HANDLER_H

#include <avr/io.h>
#include <avr/wdt.h>
#include <avr/eeprom.h>

#include "can_drv.h"
#include "global.h"

#define CAN_ID_GLB  0x18FF0000
#define CAN_ID_CMD  0x0CEA0000
#define CAN_ID_ACK  0x10E80000
#define CAN_ID_REQ  0x18EA0000

#define CAN_BI_ADRESS   0x40
#define CAN_BVV_ADRESS  0x77
#define CAN_AS_ADDRESS	0xA0

#define TO_BI			(CAN_BI_ADRESS)

// NAME GLOBAL SEND MESSAGE
#define SM_WORK_PARAM_0    0x01
#define SM_WORK_PARAM_1    0x02
#define SM_WORK_PARAM_2    0x03
#define SM_VERSION_PO      0xEE

// NAME GLOBAL RECEPTION MESSAGE
#define RM_BI_SPAM        0xDD

// NAME COMMAND IN
#define NULL_ANGELS_YES  	0x46
#define NULL_ANGELS_NO		0x47
#define DEC_CONST_CONSUPLATION  0x12

// prototype function
void canSendMes(uint8_t name_mes);
void canSendCommand(uint8_t dest, uint8_t ids, uint32_t value);
void canSendRequest(uint8_t dest, uint8_t ids);
void canMainHandler(CanMsg *msg);

#endif //__CAN_HANDLER_H