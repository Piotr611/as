////////////////////////////////////////////////////////////////////////////////
//
// File: global.c
// Description: skv_bu_mega16m1
// Author(s): RiSuS as an engineer "SKB WEST"
// Copyright(s):
// Version:
// Creation date: 2-July-2015
//
////////////////////////////////////////////////////////////////////////////////

#include "global.h"

uint8_t versionPo[4] = { 0, 9, 1, 1};

uint8_t flag_bi_enabled = 0;
uint8_t counter_bi_disabled = 0;
uint8_t flag_can = 0;
uint16_t counter_ADC_no_data = 0;
uint16_t counter_ADC_reset = 0;

const unsigned char PARAMETR_X = 1;
const unsigned char PARAMETR_Y = 2;
const unsigned char PARAMETR_Z = 3;
const float RAD_TO_DEG = 57.29578;

int zeroForAngelX = 0;
int zeroForAngelY = 0;
int zeroForAngelZ = 0;

int16_t angle_x = 0;
int16_t angle_y = 0;
int16_t angle_z = 0;

int16_t current_angle_x = 0;
int16_t current_angle_y = 0;
int16_t current_angle_z = 0;

int16_t protaction_x = 0;
int16_t protaction_y = 0;
int16_t protaction_z = 0;

int16_t value_x = 0;
int16_t value_y = 0;
int16_t value_z = 0;

//functions
void yesZeroAngels () {	
	zeroForAngelX = current_angle_x;	
	zeroForAngelY = current_angle_y;
	zeroForAngelZ = current_angle_z;	 
}

void noZeroAngels (void) {	
	zeroForAngelX = 0;
	zeroForAngelY = 0;
	zeroForAngelZ = 0;	
}


