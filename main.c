/* ********************************************************************
simple program to test and see if CAN is able to transmit a 
message over the CANBUS
********************************************************************/

#include <math.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/signal.h> 
#include <util/delay.h>

#include "canhandler.h"
#include "can_drv.h"
#include "global.h"
#include "timer0.h"

void chip_init(void);
void init_ADC(void);
void canSpamer(void);
unsigned int insertXYZ (unsigned char); 
void calculate (void);

CanMsg tx_can_mess_angels;
CanMsg tx_can_mess_protactions;
CanMsg tx_can_mess_ADC;
CanMsg tx_can_mess_version;

//****************************************************************
//***** main *****************************************************
//****************************************************************
int main(void){ 
	
    chip_init();	// Chip initialization

 	init_can(); 	// Can initialization
	
	//������� PB2 ����� ������ 3V �� ������
	DDRB |= (1<<DDB2);
	PORTB |= (1<<PB2);
	
	init_ADC();//������������� ���
	timer0_init();	
	
	sei();
		
 	while ( 1 ) {	// Forever		
		
		value_x = insertXYZ(PARAMETR_X);			
		value_y = insertXYZ(PARAMETR_Y);		
		value_z = insertXYZ(PARAMETR_Z);		
		calculate();		
		canSpamer();	
	
	}

	return(0);

}

//////////////////////////////////////////////////////////////////////////
//functions for koordinate calculations//////////////////////////////////
/////////////////////////////////////////////////////////////////////////
unsigned int insertXYZ (unsigned char parametr) {

	unsigned int value, temp, temp_2, value_summ = 0;
	int i,j,k;
	unsigned int a[COUNT_ADC];
		
	switch(parametr) {
		case 1:
			ADMUX &= ( ~( (1<<MUX4) | (1<<MUX3) | (1<<MUX2) | (1<<MUX1) | (1<<MUX0) ) );			
			ADMUX |= ((1 << MUX3) | (1 << MUX0));			
			break;
		case 2: 
			ADMUX &= ( ~( (1<<MUX4) | (1<<MUX3) | (1<<MUX2) | (1<<MUX1) | (1<<MUX0) ) );
			ADMUX |= ((1 << MUX3) | (1 << MUX1));
			break;
		case 3: 
			ADMUX &= ( ~( (1<<MUX4) | (1<<MUX3) | (1<<MUX2) | (1<<MUX1) | (1<<MUX0) ) );
			ADMUX |= ((1 << MUX2) | (1 << MUX1));
			break;
		default:
			break;
		}
	
	_delay_ms(10);	
	
	for (i=0; i<COUNT_ADC; i++ ) {
		a[i] = 0;	
		}	
	
	for (i=0; i<COUNT_HOLOST_WAY; i++ ) {
		StartADC();	
		while (bit_is_clear(ADCSRA,ADIF))
			; // ���� ���� ���������� �������������� ���			
		value = ADCW;		
		}		
	
	
	for (i=0; i<COUNT_ADC; i++ ) {
		StartADC();	
		while (bit_is_clear(ADCSRA,ADIF))
			; // ���� ���� ���������� �������������� ���			
		value = ADCW;		
		value &= 0xFFFE;//�������� 2 ������� ����
		//������� ������ � ������ �� �����������
		for (j = 0; j < COUNT_ADC; j++) {
			if (value<a[j]) {
				continue;
			} else {
				temp = a[j];
				a[j] = value;				
				for (k = j+1; k < COUNT_ADC; k++) {
					if (a[k] != 0){
						temp_2 = a[k];
						a[k] = temp;
						temp = temp_2;
						}
					else {
						break;
					}
				}
				break;				
			}		 		
		}
		_delay_ms(2);
	}
	
	for (i=DELETE_ALEMENTS; i<(COUNT_ADC-DELETE_ALEMENTS); i++ ) { 
		value_summ+=value;
	}
	value = value_summ/(COUNT_ADC-DELETE_ALEMENTS*2);
	value &= 0xFFFE;//�������� 2 ������� ����	
	return value;
		
}

void calculate () {
		
	//int16_t protaction_x, protaction_y, protaction_z;
	//int16_t angle_x, angle_y, angle_z;
	
	float xv=(value_x/analog_resolution*ADC_ref-zero_x)/sensitivity_x;
	float yv=(value_y/analog_resolution*ADC_ref-zero_y)/sensitivity_y;
	float zv=(value_z/analog_resolution*ADC_ref-zero_z)/sensitivity_z;
					
	current_angle_z = (int16_t)(atan2(-yv,-xv)*RAD_TO_DEG);	
	current_angle_y = (int16_t)(atan2(-xv,-zv)*RAD_TO_DEG);
	current_angle_x = (int16_t)(atan2(-yv,-zv)*RAD_TO_DEG);
	
	angle_z = current_angle_z - zeroForAngelZ;
	angle_y = current_angle_y - zeroForAngelY;
	angle_x = current_angle_x - zeroForAngelX;

	protaction_x = (int16_t)(xv*100);
	protaction_y = (int16_t)(yv*100);
	protaction_z = (int16_t)(zv*100);
	
	tx_can_mess_angels.data[0] = (angle_x)>>8;
	tx_can_mess_angels.data[1] = angle_x & 0xFF;
	//*(unsigned int *)(&tx_can_mess.data[1]) = (unsigned int)(angle_x);	
	tx_can_mess_angels.data[2] = (angle_y)>>8;	
	tx_can_mess_angels.data[3] = angle_y & 0xFF;	
	tx_can_mess_angels.data[4] = (angle_z)>>8;	
	tx_can_mess_angels.data[5] = angle_z & 0xFF;

	
	tx_can_mess_protactions.data[0] = (protaction_x)>>8;
	tx_can_mess_protactions.data[1] = protaction_x & 0xFF;
	tx_can_mess_protactions.data[2] = (protaction_y)>>8;
	tx_can_mess_protactions.data[3] = protaction_y & 0xFF;
	tx_can_mess_protactions.data[4] = (protaction_z)>>8;
	tx_can_mess_protactions.data[5] = protaction_z & 0xFF;
	
	tx_can_mess_ADC.data[0] = (value_x)>>8;
	tx_can_mess_ADC.data[1] = value_x & 0xFF;
	tx_can_mess_ADC.data[2] = (value_y)>>8;
	tx_can_mess_ADC.data[3] = value_y & 0xFF;
	tx_can_mess_ADC.data[4] = (value_z)>>8;
	tx_can_mess_ADC.data[5] = value_z & 0xFF;
	
}
///////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
//functions for initializations///////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////
void chip_init(void)
{
DDRC = 0x00;    // Inputs, not used
DDRD = 0x00; 	// Inputs, not used
DDRE = 0x00;	// Inputs, not used
//PORTB = 0xFE;	// ALL LEDs OFF
PORTC = 0x00;	// Inputs, not used
PORTD = 0x00;	// Inputs, not used
PORTE = 0x00;	// Inputs, not used
//PRR = 0x00;	// Individual peripheral clocks enabled 
}

void init_ADC(void) {	

	ADMUX  = ((1 << REFS1) | (1 << REFS0));		
	ADCSRB = (0 << AREFEN);	
	ADCSRA = ((1 << ADEN) | (0 << ADATE) | (0 << ADIE) | (1 << ADPS1));	
	
}
///////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////
//function for CAN/////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
void canSpamer(void)
{
	static uint8_t index=0;
	
	if(flag_can) {
		flag_can = 0;
		switch(index) {
			case 0:
				canSendMes(SM_WORK_PARAM_0);
				index++;
				break;
			case 1:
				canSendMes(SM_WORK_PARAM_1);
				index++;
				break;
			case 2:
				canSendMes(SM_WORK_PARAM_2);
				index = 0;
				break;			
			default:
				break;
		}
	}
	
}
///////////////////////////////////////////////////////////////////////////