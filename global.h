////////////////////////////////////////////////////////////////////////////////
//
// File: global.h
// Description: skv_bu_mega16m1
// Author(s): RiSuS as an engineer "SKB WEST"
// Copyright(s):
// Version:
// Creation date: 2-July-2015
//
////////////////////////////////////////////////////////////////////////////////

#ifndef __GLOBAL_H
#define __GLOBAL_H

#include <avr/io.h>
#include <avr/wdt.h>

#define F_CPU   	16000000UL
#define StartADC()  ADCSRA |= (1<<ADSC) 

#define SEND_ANGELS			0
#define SEND_PROTACTIONS 	1
#define SEND_ADC 			2
#define SEND_VERSION		3

#define VERSION_PROGRAM  	100

#define COUNT_ADC 			128
#define DELETE_ALEMENTS 	48
#define COUNT_HOLOST_WAY 	3
#define ADC_ref 			2.56
#define analog_resolution 	1024.0
#define zero_x 				1.569
#define zero_y 				1.569
#define zero_z 				1.569
#define sensitivity_x 		0.3
#define sensitivity_y 		0.3
#define sensitivity_z 		0.3

extern uint8_t versionPo[4];		//������� ������ ��

extern uint8_t flag_bi_enabled;		//���� ������� ��
extern uint8_t counter_bi_disabled; //������� ���������� ��
extern uint8_t flag_can;			//���� �������� �������� can ���������
extern uint16_t counter_ADC_no_data;
extern uint16_t counter_ADC_reset;  //������� ������� ADC

extern const unsigned char PARAMETR_X;
extern const unsigned char PARAMETR_Y;
extern const unsigned char PARAMETR_Z;
extern const float RAD_TO_DEG;

extern int zeroForAngelX;
extern int zeroForAngelY;
extern int zeroForAngelZ;

extern int16_t angle_x;
extern int16_t angle_y;
extern int16_t angle_z;

extern int16_t current_angle_x;
extern int16_t current_angle_y;
extern int16_t current_angle_z;

extern int16_t protaction_x;
extern int16_t protaction_y;
extern int16_t protaction_z;

extern int16_t value_x;
extern int16_t value_y;
extern int16_t value_z;

//functions prototype
void yesZeroAngels (void);
void noZeroAngels (void);

#endif //__GLOBAL_H