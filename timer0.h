#ifndef _TIMER0_H_
#define _TIMER0_H_

#include <inttypes.h>
#include <avr/io.h>
#include <avr/signal.h>
#include "global.h"

// 16000000(�����)/64(��������)/2000(������ ������� ������������) = 125
#define TCNT0_INIT (0x100-125)
#define IMP_PER_MS	2

void timer0_init(void);

#endif

