////////////////////////////////////////////////////////////////////////////////
//
// File: canhandler.c
// Description: skv_bu_mega16m1
// Author(s): RiSuS as an engineer "SKB WEST"
// Copyright(s):
// Version:
// Creation date: 2-July-2015
//
////////////////////////////////////////////////////////////////////////////////

#include "canhandler.h"

void canSendMes(uint8_t name_mes)
{
	uint8_t fSend = 1;	
	CanMsg txMessage;
  
	txMessage.id = CAN_ID_GLB | ((uint16_t)name_mes<<8) | CAN_AS_ADDRESS;
	txMessage.ide = CAN_ID_EXT;
	txMessage.dlc = 8;
	txMessage.data[0] = 0xFF;
	txMessage.data[1] = 0xFF;
	txMessage.data[2] = 0xFF;
	txMessage.data[3] = 0xFF;
	txMessage.data[4] = 0xFF;
	txMessage.data[5] = 0xFF;
	txMessage.data[6] = 0xFF;
	txMessage.data[7] = 0xFF;
  
	switch(name_mes) {
  
		case SM_WORK_PARAM_0:
			*(uint16_t *)(&txMessage.data[0]) = angle_x;
			*(uint16_t *)(&txMessage.data[2]) = angle_y;
			*(uint16_t *)(&txMessage.data[4]) = angle_z;
			break;
  
		case SM_WORK_PARAM_1:
			*(uint16_t *)(&txMessage.data[0]) = protaction_x;
			*(uint16_t *)(&txMessage.data[2]) = protaction_y;
			*(uint16_t *)(&txMessage.data[4]) = protaction_z;
			break;
		
		case SM_WORK_PARAM_2:
			*(uint16_t *)(&txMessage.data[0]) = value_x;
			*(uint16_t *)(&txMessage.data[2]) = value_y;
			*(uint16_t *)(&txMessage.data[4]) = value_z;
			break;		
		
		case SM_VERSION_PO:
			txMessage.data[0] = versionPo[0];
			txMessage.data[1] = versionPo[1];
			txMessage.data[2] = versionPo[2];
			txMessage.data[3] = versionPo[3];
			break;
		
		default:
			fSend = 0;
			break;
	}
  
	if(fSend) {
		can_tx(&txMessage);
	}
}

void canSendCommand(uint8_t dest, uint8_t ids, uint32_t value)
{
	CanMsg txMessage;
  
	txMessage.id = CAN_ID_CMD | ((uint16_t)dest<<8) | CAN_AS_ADDRESS;
	txMessage.ide = CAN_ID_EXT;
	txMessage.dlc = 8;
	txMessage.data[0] = ids;
	txMessage.data[1] = 0xFF;
	txMessage.data[2] = 0xFF;
	txMessage.data[3] = 0xFF;
	txMessage.data[4] = 0xFF;
	txMessage.data[5] = 0xFF;
	txMessage.data[6] = 0xFF;
	txMessage.data[7] = 0xFF;
	
	switch(ids) {
		default:
			break;
	}
	
	can_tx(&txMessage);
}

void canSendRequest(uint8_t dest, uint8_t ids)
{
	CanMsg txMessage;
  
	txMessage.id = CAN_ID_REQ | ((uint16_t)dest<<8) | CAN_AS_ADDRESS;
	txMessage.ide = CAN_ID_EXT;
	txMessage.dlc = 8;
	txMessage.data[0] = ids;
	txMessage.data[1] = 0xFF;
	txMessage.data[2] = 0xFF;
	txMessage.data[3] = 0xFF;
	txMessage.data[4] = 0xFF;
	txMessage.data[5] = 0xFF;
	txMessage.data[6] = 0xFF;
	txMessage.data[7] = 0xFF;
	
	can_tx(&txMessage);
}

void bi_data_processing(CanMsg* msg)
{
	uint8_t ids;
	
	ids = (uint8_t)((msg->id&0xFF00) >> 8);
	
	switch(ids) {
		case RM_BI_SPAM:
			flag_bi_enabled = 1;
			counter_bi_disabled = 0;
			break;
			
		default:
			break;
	}
}


#define BOOT_SECTION  0x1800
void(*pBootloader)(void) ;
void commandProcessing(CanMsg* msg)
{
	uint8_t result = 0xFE;
	uint8_t sa;
	uint8_t command;
	uint8_t buf8;
	uint16_t buf16;
	uint32_t buf32;
	CanMsg answer;
	
	
	sa = (uint8_t)(msg->id&0xFF);
	command = msg->data[0];
	buf8 = msg->data[1];
	buf16 = *(uint16_t *)(&msg->data[1]);
	buf32 = *(uint32_t *)(&msg->data[1]);
	
	switch(command) {	
			
		case NULL_ANGELS_YES:
			yesZeroAngels();
			result = 0x00;
			break;
			
		case NULL_ANGELS_NO:
			noZeroAngels();
			result = 0x00;
			break;		
				
		default:
			break;
	}
	
	
	/* TEST
	if (consumption_l_per_min_x100_set > 100) flag_noDRDY = 1;
	else flag_noDRDY = 0;*/
	answer.id = CAN_ID_ACK | CAN_AS_ADDRESS | ((uint16_t)sa << 8);
	answer.ide = CAN_ID_EXT;
	answer.dlc = 8;
	answer.data[0] = command;
	answer.data[1] = result;
	answer.data[2] = 0xFF;
	answer.data[3] = 0xFF;
	answer.data[4] = 0xFF;
	answer.data[5] = 0xFF;
	answer.data[6] = 0xFF;
	answer.data[7] = 0xFF;
	
	can_tx(&answer);
	
}

void canMainHandler(CanMsg* msg)
{
	uint32_t msg_id, msg_type, msg_receiver, msg_sender;
	
	msg_id = msg->id;
	msg_type = msg_id & 0xFFFF0000;
	msg_receiver = (msg_id&0x0000FF00)>>8;
	msg_sender = msg_id&0xFF;
	
	if( (msg_type == CAN_ID_GLB) || (msg_receiver == CAN_AS_ADDRESS) ) {
		switch ( msg_type ) {
			case CAN_ID_GLB:
				if(msg_sender == CAN_BI_ADRESS) {
				
					//flag_bi_enabled = 1;
					//counter_bi_disabled = 0;
					
					bi_data_processing(msg);
				}
				break;
				
			case CAN_ID_CMD:
				commandProcessing(msg);
				break;
				
			case CAN_ID_ACK:
				// To do: �������� ������ ������� �� �������
				break;
			
			case CAN_ID_REQ:
			  canSendMes(msg->data[0]);
			  break;
			  
			default:
			  break;
		}
	 }
}