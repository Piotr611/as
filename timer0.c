#include "timer0.h"

volatile uint16_t ipsCounter = 0;
volatile uint8_t t8ms = 0;
volatile uint16_t tcan = 0;
volatile uint16_t t250ms = 0;
volatile uint16_t t1s = 0;
volatile uint16_t t2s = 0;
//int zazaza = 0;
	
void timer0_init(void)
{
	TCCR0A = 0;
	TCCR0B = (0<<CS12)|(1<<CS11)|(1<<CS10); // clk
	TIMSK0 = (1<<TOIE0);
	TCNT0 = TCNT0_INIT;
}

ISR (TIMER0_OVF_vect)
{
	t1s++;
	t8ms++;
	tcan++;
	counter_ADC_no_data++;
	//if(delay_dgPWM) t250ms++;
	
	TCNT0 = TCNT0_INIT;
	
	if(tcan >=(IMP_PER_MS*100)) {
		tcan = 0;
		flag_can = 1;
	}	
	
	wdt_reset();
}